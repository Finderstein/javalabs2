package perehuda;

import org.junit.jupiter.api.Test;
import perehuda.FS.BinaryFile;
import perehuda.FS.Directory;

import static org.junit.jupiter.api.Assertions.*;
import static org.junit.jupiter.api.Assertions.assertTrue;

public class BinaryFileTests {
    @Test
    void createBinaryFileTest() {
        Directory root = Directory.create("root", null);
        BinaryFile file = BinaryFile.create("file", root, "information");

        assertNull(BinaryFile.create("Abrakadabra", null, "sad"));
        assertNull(BinaryFile.create("", root, "sad"));
        assertNull(BinaryFile.create("/", root, "sad"));

        assertNotNull(file);
        assertEquals("file", file.getName());
        assertEquals("information", file.getData());
        assertEquals("BinaryFile", file.getType());
    }

    @Test
    void moveBinaryFileTest() {
        Directory root = Directory.create("root", null);
        Directory subDir1 = Directory.create("subDir1", root);
        Directory subDir2 = Directory.create("subDir2", root);
        BinaryFile file = BinaryFile.create("file", subDir1, "asd");

        assertTrue(root.getFiles().contains(subDir1));
        assertTrue(root.getFiles().contains(subDir2));
        assertTrue(subDir1.getFiles().contains(file));

        file.moveTo(subDir2);

        assertFalse(subDir1.getFiles().contains(file));
        assertTrue(root.getFiles().contains(subDir2));
        assertTrue(root.getFiles().contains(subDir1));
        assertTrue(subDir2.getFiles().contains(file));
    }

    @Test
    void deleteBinaryTest() {
        Directory root = Directory.create("root", null);
        Directory subDir1 = Directory.create("subDir1", root);
        BinaryFile file1 = BinaryFile.create("file1", root, "info");
        BinaryFile file2 = BinaryFile.create("file2", subDir1, "info");

        assertTrue(root.getFiles().contains(file1));
        assertTrue(subDir1.getFiles().contains(file2));

        file1.delete();
        file2.delete();

        assertFalse(root.getFiles().contains(file1));
        assertFalse(subDir1.getFiles().contains(file2));
    }

    @Test
    void readBinaryFileTest() {
        Directory root = Directory.create("root", null);
        BinaryFile file1 = BinaryFile.create("file1", root, "Data");
        BinaryFile file2 = BinaryFile.create("file2", root, "");

        assertEquals("Data", file1.getData());
        assertEquals("", file2.getData());
    }
}
