package perehuda;

import org.junit.jupiter.api.Test;
import perehuda.FS.Directory;
import perehuda.FS.LogFile;

import static org.junit.jupiter.api.Assertions.*;
import static org.junit.jupiter.api.Assertions.assertFalse;

public class LogFileTests {
    @Test
    void createLogFileTest() {
        Directory root = Directory.create("root", null);
        LogFile file = LogFile.create("file", root, "information");

        assertNull(LogFile.create("Abrakadabra", null, "sad"));
        assertNull(LogFile.create("", root, "sad"));
        assertNull(LogFile.create("/", root, "sad"));

        assertNotNull(file);
        assertEquals("file", file.getName());
        assertEquals("information", file.getData());
        assertEquals("LogFile", file.getType());
    }

    @Test
    void moveLogFileTest() {
        Directory root = Directory.create("root", null);
        Directory subDir1 = Directory.create("subDir1", root);
        Directory subDir2 = Directory.create("subDir2", root);
        LogFile file = LogFile.create("file", subDir1, "info");

        assertTrue(root.getFiles().contains(subDir1));
        assertTrue(root.getFiles().contains(subDir2));
        assertTrue(subDir1.getFiles().contains(file));

        file.moveTo(subDir2);

        assertFalse(subDir1.getFiles().contains(file));
        assertTrue(root.getFiles().contains(subDir2));
        assertTrue(root.getFiles().contains(subDir1));
        assertTrue(subDir2.getFiles().contains(file));
    }

    @Test
    void deleteLogFileTest() {
        Directory root = Directory.create("root", null);
        Directory subDir1 = Directory.create("subDir1", root);
        LogFile file1 = LogFile.create("file1", root, "info");
        LogFile file2 = LogFile.create("file2", subDir1, "info");

        assertTrue(root.getFiles().contains(file1));
        assertTrue(subDir1.getFiles().contains(file2));

        file1.delete();
        file2.delete();

        assertFalse(root.getFiles().contains(file1));
        assertFalse(subDir1.getFiles().contains(file2));
    }

    @Test
    void readLogFileTest() {
        Directory root = Directory.create("root", null);
        LogFile file1 = LogFile.create("file1", root, "Data");
        LogFile file2 = LogFile.create("file2", root, "");

        assertEquals("Data", file1.getData());
        assertEquals("", file2.getData());

        file1.append(" More data");
        file2.append("Some data");

        assertEquals("Data More data", file1.getData());
        assertEquals("Some data", file2.getData());
    }
}
