package perehuda;

import perehuda.FS.*;

public class Task2 {
    public static void main(String[] args) {
        Directory root = Directory.create("root", null);
        Directory dir1 = Directory.create("dir1", root);
        Directory dir2 = Directory.create("dir2", root);
        Directory dir3 = Directory.create("dir3", root);
        Directory subDir1 = Directory.create("subDir1", dir2);
        Directory subDir2 = Directory.create("subDir2", dir2);
        Directory subDir3 = Directory.create("subDir3", dir3);
        Directory subSubDir1 = Directory.create("subSubDir1", subDir1);

        BinaryFile binFile1 = BinaryFile.create("BinFile1.bin", dir1, "This is first BinaryFile");
        BinaryFile binFile2 = BinaryFile.create("BinFile2.bin", dir1, "This is second BinaryFile");
        BinaryFile binFile3 = BinaryFile.create("BinFile3.bin", dir3, "This is third BinaryFile");
        BinaryFile binFile4 = BinaryFile.create("BinFile4.bin", subDir1, "This is fourth BinaryFile");
        BinaryFile binFile5 = BinaryFile.create("BinFile5.bin", subSubDir1, "This is fifth BinaryFile");
        BinaryFile binFile6 = BinaryFile.create("BinFile6.bin", subDir3, "This is sixth BinaryFile");

        BufferFile buffFile1 = BufferFile.<Integer>create("BuffFile1.buff", dir1);
        BufferFile buffFile2 = BufferFile.<Integer>create("BuffFile2.buff", subDir2);

        LogFile logFile1 = LogFile.create("LogFile1.log", dir3, "Some info");
        LogFile logFile2 = LogFile.create("LogFile2.log", subSubDir1, "Another info");

        MyFile myFile1 = MyFile.create("MyFile1.my", subDir1, "25");
        MyFile myFile2 = MyFile.create("MyFile2.my", dir1, "13");
        MyFile myFile3 = MyFile.create("MyFile3.my", subDir3, "74");

        Directory.showAllFiles(root, "");
    }
}
