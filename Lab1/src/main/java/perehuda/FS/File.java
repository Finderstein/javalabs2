package perehuda.FS;

public class File {
    private final String type, name;
    private Directory parent;

    public File(String type, String name, Directory parent) {
        this.type = type;
        this.name = name;
        this.parent = parent;
    }

    public String getType() {
        return type;
    }

    public String getName() {
        return name;
    }

    public synchronized Directory getParent() {
        return parent;
    }

    public synchronized boolean delete() {
        if(parent == null) return false;
        parent.removeFile(this);
        parent = null;
        return true;
    }

    public synchronized boolean moveTo(Directory dir) {
        parent.removeFile(this);
        parent = dir;
        return dir.addFile(this);
    }
}
