package perehuda.FS;

import java.util.concurrent.atomic.AtomicReference;

public class MyFile extends File {
    private AtomicReference<String> data;

    public MyFile(String name, Directory parent, String data) {
        super("MyFile", name, parent);
        this.data = new AtomicReference<>(data);
        parent.addFile(this);
    }

    public static MyFile create(String name, Directory parent, String data) {
        if(name.equals("") || name.equals("/") || parent == null || parent.full()) return null;

        return new MyFile(name, parent, data);
    }

    public String getNum() {
        return data.get();
    }

    public void add(String data) {
        this.data.set(Integer.toString(Integer.parseInt(data) + Integer.parseInt(getNum())));
    }
}
