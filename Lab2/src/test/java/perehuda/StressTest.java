package perehuda;

import org.junit.jupiter.api.Test;
import perehuda.lab2_tasks.Generator;

import java.io.IOException;
import java.util.concurrent.ExecutionException;

import static org.junit.jupiter.api.Assertions.assertTrue;

public class StressTest {
    @Test
    void stressTest() throws InterruptedException, ExecutionException, IOException {
        Generator generator = new Generator();

        assertTrue(generator.doStressTest(100000));
    }
}
