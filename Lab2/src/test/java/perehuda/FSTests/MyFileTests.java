package perehuda.FSTests;

import org.junit.jupiter.api.Test;
import perehuda.FS.Directory;
import perehuda.FS.MyFile;

import static org.junit.jupiter.api.Assertions.*;
import static org.junit.jupiter.api.Assertions.assertFalse;

public class MyFileTests {
    @Test
    void createBinaryFileTest() {
        Directory root = Directory.create("root", null);
        MyFile file = MyFile.create("file", root, "25");

        assertNull(MyFile.create("Abrakadabra", null, "25"));
        assertNull(MyFile.create("", root, "25"));
        assertNull(MyFile.create("/", root, "25"));

        assertNotNull(file);
        assertEquals("file", file.getName());
        assertEquals("25", file.getNum());
        assertEquals("MyFile", file.getType());
    }

    @Test
    void moveBinaryFileTest() {
        Directory root = Directory.create("root", null);
        Directory subDir1 = Directory.create("subDir1", root);
        Directory subDir2 = Directory.create("subDir2", root);
        MyFile file = MyFile.create("file", subDir1, "25");

        assertTrue(root.getFiles().contains(subDir1));
        assertTrue(root.getFiles().contains(subDir2));
        assertTrue(subDir1.getFiles().contains(file));

        file.moveTo(subDir2);

        assertFalse(subDir1.getFiles().contains(file));
        assertTrue(root.getFiles().contains(subDir2));
        assertTrue(root.getFiles().contains(subDir1));
        assertTrue(subDir2.getFiles().contains(file));
    }

    @Test
    void deleteBinaryTest() {
        Directory root = Directory.create("root", null);
        Directory subDir1 = Directory.create("subDir1", root);
        MyFile file1 = MyFile.create("file1", root, "25");
        MyFile file2 = MyFile.create("file2", subDir1, "25");

        assertTrue(root.getFiles().contains(file1));
        assertTrue(subDir1.getFiles().contains(file2));

        file1.delete();
        file2.delete();

        assertFalse(root.getFiles().contains(file1));
        assertFalse(subDir1.getFiles().contains(file2));
    }

    @Test
    void addBinaryFileTest() {
        Directory root = Directory.create("root", null);
        MyFile file = MyFile.create("file", root, "25");

        assertEquals("25", file.getNum());

        file.add("13");

        assertEquals("38", file.getNum());
    }
}
