package perehuda.lab1_tasks;

import java.util.Random;
import java.util.concurrent.BrokenBarrierException;
import java.util.concurrent.CyclicBarrier;
import java.util.function.Function;

public class Task3 {
    private static class MyThread implements Runnable {
        long sleep1, sleep2, sleep3;
        CyclicBarrier barrier1;
        Runnable barrier2;
        String name;

        public MyThread(String name, int sleep1, int sleep2, int sleep3, CyclicBarrier barrier1) {
            this.name = name;
            this.sleep1 = sleep1;
            this.sleep2 = sleep2;
            this.sleep3 = sleep3;
            this.barrier1 = barrier1;
        }

        public void setBarrier2(Runnable barrier2) {
            this.barrier2 = barrier2;
        }

        private void task_init() {
            try {
                Thread.sleep(sleep1);
                System.out.println(name + " finished task_init()");
                barrier1.await();
            } catch (InterruptedException | BrokenBarrierException e) {
                e.printStackTrace();
            }
        }

        private void task() {
            try {
                Thread.sleep(sleep2);
                System.out.println(name + " finished task()");
                if(barrier2 != null) barrier2.run();
            } catch (InterruptedException e) {
                e.printStackTrace();
            }
        }

        private void task_finalize() {
            try {
                Thread.sleep(sleep3);
                System.out.println(name+" finished task_finalize()");
            } catch (InterruptedException e) {
                e.printStackTrace();
            }
        }

        @Override
        public void run() {
            task_init();

            task();

            task_finalize();
        }
    }

    static Runnable threadsJoin(Thread... threads) {
        return () -> {
            for(Thread thread : threads)
                try {
                    thread.join();
                } catch (InterruptedException e) {
                    e.printStackTrace();
                }
        };
    }

    public static void main(String[] args) throws InterruptedException {
        Function<Long, Integer> randomSleep = (Long seed) -> new Random(seed).nextInt(5000 - 500) + 500;
        CyclicBarrier barrier1 = new CyclicBarrier(5);
        MyThread[] myThreads = new MyThread[5];
        Thread[] threads = new Thread[5];

        for(int i = 0; i < 5; i++) {
            myThreads[i] = new MyThread(
                    "Thread " + (i + 1),
                    randomSleep.apply(99L + i * 13),
                    randomSleep.apply(99L + i * 13 + 3),
                    randomSleep.apply(99L + i * 13 + 11),
                    barrier1
            );
            threads[i] = new Thread(myThreads[i]);
        }

        myThreads[0].setBarrier2(threadsJoin(threads[2], threads[3]));
        myThreads[1].setBarrier2(threadsJoin(threads[2], threads[3]));
        myThreads[4].setBarrier2(threadsJoin(threads[0], threads[1]));

        for(Thread thread : threads)
            thread.start();
    }
}
