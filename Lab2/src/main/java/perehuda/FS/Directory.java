package perehuda.FS;

import com.fasterxml.jackson.databind.ObjectMapper;
import perehuda.FS.ForkJoinPoolHelpers.CountHelper;
import perehuda.FS.ForkJoinPoolHelpers.LongestPathHelper;
import perehuda.FS.ForkJoinPoolHelpers.SearchHelper;
import perehuda.FS.ForkJoinPoolHelpers.TreeHelper;

import java.io.IOException;
import java.util.ArrayList;
import java.util.concurrent.ForkJoinPool;

public class Directory extends File {
    private static final int DIR_MAX_ELEMS = 20;
    private ArrayList<? super File> files;

    private Directory(String name, Directory parent) {
        super("Directory", name, parent);
        files = new ArrayList<>();
        if(parent != null)
            parent.addFile(this);
    }

    public static Directory create(String name, Directory parent) {
        if(name.equals("") || name.equals("/") || (parent != null && parent.full())) return null;

        return new Directory(name, parent);
    }

    public synchronized boolean addFile(File file) {
        if(!files.contains(file) && (file != null && files.size() != DIR_MAX_ELEMS))
            files.add(file);
        else return false;
        return true;
    }

    public synchronized boolean removeFile(File file) {
        return files.remove(file);
    }

    public synchronized ArrayList<? super File> getFiles() {
        return (ArrayList<? super File>)files.clone();
    }

    public boolean full() {
        return files.size() == DIR_MAX_ELEMS;
    }

    public static void showAllFiles(Directory root, String indent) {
        System.out.println(indent + "└─" + root.getName());
        prettyPrint(root, indent);
    }

    public ArrayList<String> search(String pattern) {
        return ForkJoinPool.commonPool().invoke(new SearchHelper(this, pattern));
    }

    public Integer count(Boolean recursive) {
        if(recursive) return ForkJoinPool.commonPool().invoke(new CountHelper(this));
        return files.size();
    }

    public String tree() throws IOException {
        return new ObjectMapper().writerWithDefaultPrettyPrinter().writeValueAsString(ForkJoinPool.commonPool().invoke(new TreeHelper(this)));
    }

    public String longestPath() throws IOException {
        return ForkJoinPool.commonPool().invoke(new LongestPathHelper(this));
    }


    private int size() {
        return files.size();
    }

    private static void prettyPrint(Directory dir, String indent) {
        if(dir.size() != 0) {
            ArrayList<? super File> files = dir.getFiles();
            int size = files.size();
            for(int i = 0; i < size; i++) {
                File file = (File) files.get(i);
                if(file.getType() != "Directory")
                    if(size - i > 1)
                        System.out.println("  " + indent + "├─" + file.getName());
                    else
                        System.out.println("  " + indent + "└─" + file.getName());
                else
                {
                    if(size - i > 1) {
                        System.out.println("  " + indent + "├─" + file.getName());
                        prettyPrint((Directory) file, indent + "|  ");
                    }
                    else
                    {
                        System.out.println("  " + indent + "└─" + file.getName());
                        prettyPrint((Directory) file, indent + "  ");
                    }
                }
            }
        }
    }
}
