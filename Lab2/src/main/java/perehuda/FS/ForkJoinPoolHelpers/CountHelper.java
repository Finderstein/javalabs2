package perehuda.FS.ForkJoinPoolHelpers;

import perehuda.FS.Directory;
import perehuda.FS.File;

import java.util.ArrayList;
import java.util.concurrent.RecursiveTask;

public class CountHelper extends RecursiveTask<Integer> {
    private final Directory directory;

    public CountHelper(Directory directory) {
        this.directory = directory;
    }

    @Override
    protected Integer compute() {
        ArrayList<? super File> children = directory.getFiles();
        Integer sum = children.size();
        ArrayList<CountHelper> subHelpers = new ArrayList<>();

        for(Object object : children) {
            File child = (File) object;

            if (child.getType() == "Directory") {
                CountHelper helper = new CountHelper((Directory)child);
                helper.fork();
                subHelpers.add(helper);
            }
        }

        for(CountHelper helper : subHelpers) {
            sum += helper.join();
        }

        return sum;
    }

}