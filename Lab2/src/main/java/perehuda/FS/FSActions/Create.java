package perehuda.FS.FSActions;

import perehuda.FS.*;

import java.lang.reflect.Type;

public class Create implements FSAction<File> {
    private final Directory parent;
    private final String name;
    private final String type;
    private final String data;

    public Create(Directory parent, String name, String type, String data) {
        this.parent = parent;
        this.name = name;
        this.type = type;
        this.data = data;
    }

    public Create(Directory parent, String name, String type) {
        this.parent = parent;
        this.name = name;
        this.type = type;
        this.data = null;
    }

    @Override
    public File execute() {
        switch (type)
        {
            case "BinaryFile": return BinaryFile.create(name, parent, data);
            case "Directory": return Directory.create(name, parent);
            case "LogFile": return LogFile.create(name, parent, data);
            case "MyFile": return MyFile.create(name, parent, data);
            case "BufferFile": return BufferFile.<String>create(name, parent);
            default: return new File(type, name, parent);
        }
    }
}
