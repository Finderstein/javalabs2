package perehuda.FS.FSActions;


import perehuda.FS.Directory;

import java.util.ArrayList;

public class Search implements FSAction<ArrayList<String>> {
    private final Directory dir;
    private final String pattern;

    public Search(Directory dir, String pattern) {
        this.dir = dir;
        this.pattern = pattern;
    }

    @Override
    public ArrayList<String> execute() {
        return dir.search(pattern);
    }
}
