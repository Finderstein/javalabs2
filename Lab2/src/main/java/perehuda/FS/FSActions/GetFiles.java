package perehuda.FS.FSActions;

import perehuda.FS.Directory;
import perehuda.FS.File;

import java.util.ArrayList;

public class GetFiles implements FSAction<ArrayList<? super File>> {
    private final Directory dir;

    public GetFiles(Directory dir) {
        this.dir = dir;
    }

    @Override
    public ArrayList<? super File> execute() {
        return dir.getFiles();
    }
}
