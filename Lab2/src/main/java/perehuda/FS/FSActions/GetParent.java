package perehuda.FS.FSActions;

import perehuda.FS.Directory;
import perehuda.FS.File;

public class GetParent implements FSAction<Directory> {
    private final File file;

    public GetParent(File file) {
        this.file = file;
    }

    @Override
    public Directory execute() {
        return file.getParent();
    }
}
