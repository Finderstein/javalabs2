package perehuda.FS.FSActions;

import perehuda.FS.MyFile;

public class Add implements FSAction<Boolean> {
    private final MyFile my;
    private final String data;

    public Add(MyFile my, String data) {
        this.my = my;
        this.data = data;
    }

    @Override
    public Boolean execute() {
        return my.add(data);
    }
}
