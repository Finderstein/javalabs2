package perehuda.FS.FSActions;

import perehuda.FS.MyFile;

public class GetNum implements FSAction<String> {
    private final MyFile my;

    public GetNum(MyFile my) {
        this.my = my;
    }

    @Override
    public String execute() {
        return my.getNum();
    }
}
