package perehuda.FS.FSActions;

import perehuda.FS.BufferFile;

public class Push<T> implements FSAction<Boolean> {
    private final BufferFile<T> bufferFile;
    private final T data;

    public Push(BufferFile<T> bufferFile, T data) {
        this.bufferFile = bufferFile;
        this.data = data;
    }

    @Override
    public Boolean execute() {
        return bufferFile.push(data);
    }
}
