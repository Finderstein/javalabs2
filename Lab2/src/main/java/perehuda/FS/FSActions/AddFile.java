package perehuda.FS.FSActions;

import perehuda.FS.Directory;
import perehuda.FS.File;

public class AddFile implements FSAction<Boolean> {
    private final Directory dir;
    private final File file;

    public AddFile(Directory dir, File file) {
        this.dir = dir;
        this.file = file;
    }

    @Override
    public Boolean execute() {
        return dir.addFile(file);
    }
}
