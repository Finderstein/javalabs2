package perehuda.FS.FSActions;

import perehuda.FS.Directory;

import java.io.IOException;

public class Tree implements FSAction<String> {
    private final Directory directory;

    public Tree(Directory directory) {
        this.directory = directory;
    }

    @Override
    public String execute() throws IOException {
        return directory.tree();
    }
}
