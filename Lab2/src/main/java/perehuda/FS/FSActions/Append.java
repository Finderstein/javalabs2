package perehuda.FS.FSActions;

import perehuda.FS.LogFile;

public class Append implements FSAction<Boolean> {
    private final LogFile log;
    private final String data;

    public Append(LogFile log, String data) {
        this.log = log;
        this.data = data;
    }

    @Override
    public Boolean execute() {
        return log.append(data);
    }
}
