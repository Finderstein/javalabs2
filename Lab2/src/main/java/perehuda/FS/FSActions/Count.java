package perehuda.FS.FSActions;

import perehuda.FS.Directory;

public class Count implements FSAction<Integer> {
    private final Directory directory;
    private final Boolean recursive;

    public Count(Directory directory, Boolean recursive) {
        this.directory = directory;
        this.recursive = recursive;
    }

    @Override
    public Integer execute() {
        return directory.count(recursive);
    }
}
