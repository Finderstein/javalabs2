package perehuda.FS.FSActions;

import perehuda.FS.LogFile;

public class GetLogData implements FSAction<String> {
    private final LogFile log;

    public GetLogData(LogFile log) {
        this.log = log;
    }

    @Override
    public String execute() {
        return log.getLogs();
    }
}
