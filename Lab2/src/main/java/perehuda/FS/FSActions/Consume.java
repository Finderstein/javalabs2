package perehuda.FS.FSActions;

import perehuda.FS.BufferFile;

public class Consume<T> implements FSAction<T> {
    private final BufferFile<T> bufferFile;

    public Consume(BufferFile<T> bufferFile) {
        this.bufferFile = bufferFile;
    }

    @Override
    public T execute() {
        return bufferFile.consume();
    }
}
