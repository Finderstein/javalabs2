package perehuda.FS.FSActions;

import perehuda.FS.File;

public class Delete implements FSAction<Boolean> {
    private final File file;

    public Delete(File file) {
        this.file = file;
    }

    @Override
    public Boolean execute() {
        return file.delete();
    }
}
