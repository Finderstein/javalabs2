package perehuda.FS.FSActions;

import java.io.IOException;

public interface FSAction<T> {
    T execute() throws IOException;
}
