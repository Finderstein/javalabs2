package perehuda.FS.FSActions;

import perehuda.FS.Directory;
import perehuda.FS.File;

public class MoveTo implements FSAction<Boolean> {
    private final File file;
    private final Directory dir;

    public MoveTo(File file, Directory dir) {
        this.file = file;
        this.dir = dir;
    }

    @Override
    public Boolean execute() {
        return file.moveTo(dir);
    }
}
