package perehuda.Lab3.FS.FSActions;

import perehuda.Lab3.FS.Directory;
import perehuda.Lab3.FS.File;

public class GetParent implements FSAction<Directory> {
    private final File file;

    public GetParent(File file) {
        this.file = file;
    }

    @Override
    public Directory execute() {
        return file.getParent();
    }
}
