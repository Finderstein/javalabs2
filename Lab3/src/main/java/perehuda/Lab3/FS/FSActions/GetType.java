package perehuda.Lab3.FS.FSActions;

import perehuda.Lab3.FS.File;

public class GetType implements FSAction<String> {
    private final File file;

    public GetType(File file) {
        this.file = file;
    }

    @Override
    public String execute() {
        return file.getType();
    }
}
