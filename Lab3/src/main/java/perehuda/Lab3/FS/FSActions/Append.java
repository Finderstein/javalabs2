package perehuda.Lab3.FS.FSActions;

import perehuda.Lab3.FS.LogFile;

public class Append implements FSAction<Boolean> {
    private final LogFile log;
    private final String data;

    public Append(LogFile log, String data) {
        this.log = log;
        this.data = data;
    }

    @Override
    public Boolean execute() {
        return log.append(data);
    }
}
