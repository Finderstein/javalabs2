package perehuda.Lab3.FS.FSActions;

import perehuda.Lab3.FS.Directory;
import perehuda.Lab3.FS.File;

public class RemoveFile implements FSAction<Boolean> {
    private final Directory dir;
    private final File file;

    public RemoveFile(Directory dir, File file) {
        this.dir = dir;
        this.file = file;
    }

    @Override
    public Boolean execute() {
        return dir.removeFile(file);
    }
}
