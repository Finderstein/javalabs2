package perehuda.Lab3.FS.FSActions;

import perehuda.Lab3.FS.LogFile;

public class GetLogData implements FSAction<String> {
    private final LogFile log;

    public GetLogData(LogFile log) {
        this.log = log;
    }

    @Override
    public String execute() {
        return log.getLogs();
    }
}
