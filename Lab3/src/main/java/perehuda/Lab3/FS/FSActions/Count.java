package perehuda.Lab3.FS.FSActions;

import perehuda.Lab3.FS.Directory;

public class Count implements FSAction<Integer> {
    private final Directory directory;
    private final Boolean recursive;

    public Count(Directory directory, Boolean recursive) {
        this.directory = directory;
        this.recursive = recursive;
    }

    @Override
    public Integer execute() {
        return directory.count(recursive);
    }
}
