package perehuda.Lab3.FS.FSActions;

import perehuda.Lab3.FS.Directory;
import perehuda.Lab3.FS.File;

public class AddFile implements FSAction<Boolean> {
    private final Directory dir;
    private final File file;

    public AddFile(Directory dir, File file) {
        this.dir = dir;
        this.file = file;
    }

    @Override
    public Boolean execute() {
        return dir.addFile(file);
    }
}
