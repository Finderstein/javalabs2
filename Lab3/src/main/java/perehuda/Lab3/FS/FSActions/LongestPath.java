package perehuda.Lab3.FS.FSActions;

import perehuda.Lab3.FS.Directory;

import java.io.IOException;

public class LongestPath implements FSAction<String> {
    private final Directory directory;

    public LongestPath(Directory directory) {
        this.directory = directory;
    }

    @Override
    public String execute() throws IOException {
        return directory.longestPath();
    }
}