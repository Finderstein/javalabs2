package perehuda.Lab3.FS.FSActions;

import perehuda.Lab3.FS.Directory;
import perehuda.Lab3.FS.File;

public class MoveTo implements FSAction<Boolean> {
    private final File file;
    private final Directory dir;

    public MoveTo(File file, Directory dir) {
        this.file = file;
        this.dir = dir;
    }

    @Override
    public Boolean execute() {
        return file.moveTo(dir);
    }
}
