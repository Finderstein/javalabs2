package perehuda.Lab3.FS.FSActions;

import perehuda.Lab3.FS.Directory;
import perehuda.Lab3.FS.File;

import java.util.ArrayList;

public class GetFiles implements FSAction<ArrayList<? super File>> {
    private final Directory dir;

    public GetFiles(Directory dir) {
        this.dir = dir;
    }

    @Override
    public ArrayList<? super File> execute() {
        return dir.getFiles();
    }
}
