package perehuda.Lab3.FS.FSActions;

import perehuda.Lab3.FS.BufferFile;

public class Consume<T> implements FSAction<T> {
    private final BufferFile<T> bufferFile;

    public Consume(BufferFile<T> bufferFile) {
        this.bufferFile = bufferFile;
    }

    @Override
    public T execute() {
        return bufferFile.consume();
    }
}
