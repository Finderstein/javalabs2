package perehuda.Lab3.FS.FSActions;

import perehuda.Lab3.FS.MyFile;

public class GetNum implements FSAction<String> {
    private final MyFile my;

    public GetNum(MyFile my) {
        this.my = my;
    }

    @Override
    public String execute() {
        return my.getNum();
    }
}
