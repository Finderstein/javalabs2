package perehuda.Lab3.FS.FSActions;

import perehuda.Lab3.FS.BinaryFile;

public class GetBinaryData implements FSAction<String> {
    private final BinaryFile bin;

    public GetBinaryData(BinaryFile bin) {
        this.bin = bin;
    }

    @Override
    public String execute() {
        return bin.getData();
    }
}
