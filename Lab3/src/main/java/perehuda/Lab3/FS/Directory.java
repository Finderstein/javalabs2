package perehuda.Lab3.FS;

import com.fasterxml.jackson.databind.ObjectMapper;
import perehuda.Lab3.FS.ForkJoinPoolHelpers.*;

import java.io.IOException;
import java.util.ArrayList;
import java.util.concurrent.ForkJoinPool;

public class Directory extends File {
    private static final int DIR_MAX_ELEMS = 20;
    private static String filesTree;
    private ArrayList<? super File> files;

    private Directory(String name, Directory parent) {
        super("Directory", name, parent);
        files = new ArrayList<>();
        if(parent != null)
            parent.addFile(this);
    }

    public static Directory create(String name, Directory parent) {
        if(name.equals("") || name.equals("/") || (parent != null && parent.full())) return null;

        return new Directory(name, parent);
    }

    public static synchronized Directory filledRoot() {
        Directory root = Directory.create("root", null);
        Directory dir1 = Directory.create("dir1", root);
        Directory dir2 = Directory.create("dir2", root);
        Directory dir3 = Directory.create("dir3", root);
        Directory subDir1 = Directory.create("subDir1", dir2);
        Directory subDir2 = Directory.create("subDir2", dir2);
        Directory subDir3 = Directory.create("subDir3", dir3);
        Directory subSubDir1 = Directory.create("subSubDir1", subDir1);

        BinaryFile binFile1 = BinaryFile.create("BinFile1.bin", dir1, "This is first BinaryFile");
        BinaryFile binFile2 = BinaryFile.create("BinFile2.bin", dir1, "This is second BinaryFile");
        BinaryFile binFile3 = BinaryFile.create("BinFile3.bin", dir3, "This is third BinaryFile");
        BinaryFile binFile4 = BinaryFile.create("BinFile4.bin", subDir1, "This is fourth BinaryFile");
        BinaryFile binFile5 = BinaryFile.create("BinFile5.bin", subSubDir1, "This is fifth BinaryFile");
        BinaryFile binFile6 = BinaryFile.create("BinFile6.bin", subDir3, "This is sixth BinaryFile");

        BufferFile buffFile1 = BufferFile.<Integer>create("BuffFile1.buff", dir1);
        BufferFile buffFile2 = BufferFile.<Integer>create("BuffFile2.buff", subDir2);

        LogFile logFile1 = LogFile.create("LogFile1.log", dir3, "Some info");
        LogFile logFile2 = LogFile.create("LogFile2.log", subSubDir1, "Another info");

        MyFile myFile1 = MyFile.create("MyFile1.my", subDir1, "25");
        MyFile myFile2 = MyFile.create("MyFile2.my", dir1, "13");
        MyFile myFile3 = MyFile.create("MyFile3.my", subDir3, "74");

        return root;
    }

    public synchronized boolean addFile(File file) {
        if(!files.contains(file) && (file != null && files.size() != DIR_MAX_ELEMS))
            files.add(file);
        else return false;
        return true;
    }

    public synchronized boolean removeFile(File file) {
        return files.remove(file);
    }

    public static synchronized File findInsideFS(Directory dir, String name) {
        File result = null;
        ArrayList<? super File> files = dir.getFiles();
        for(int i = 0; i < files.size(); i++)
        {
            if(result != null) break;
            File file = (File) files.get(i);
            if(file.getName().contains(name))
            {
                return file;
            }
            else if(file.getType() == "Directory")
            {
                result = findInsideFS((Directory) file, name);
            }
        }

        return result;
    }

    public synchronized ArrayList<? super File> getFiles() {
        return (ArrayList<? super File>)files.clone();
    }

    public boolean full() {
        return files.size() == DIR_MAX_ELEMS;
    }

    public static void showAllFiles(Directory root, String indent) {
        System.out.println(indent + "└─" + root.getName());
        prettyPrint(root, indent);
    }

    public ArrayList<String> search(String pattern) {
        return ForkJoinPool.commonPool().invoke(new SearchHelper(this, pattern));
    }

    public Integer count(Boolean recursive) {
        if(recursive) return ForkJoinPool.commonPool().invoke(new CountHelper(this));
        return files.size();
    }

    public String tree() throws IOException {
        return new ObjectMapper().writerWithDefaultPrettyPrinter().writeValueAsString(ForkJoinPool.commonPool().invoke(new TreeHelper(this)));
    }

    public String longestPath() throws IOException {
        return ForkJoinPool.commonPool().invoke(new LongestPathHelper(this));
    }


    private int size() {
        return files.size();
    }

    public static String filesTree(Directory root, String indent) {
        filesTree = indent + "└─" + root.getName() + "\n";
        prettyFilesTree(root, indent);
        return  filesTree;
    }

    private static void prettyFilesTree(Directory dir, String indent) {
        if(dir.size() != 0) {
            ArrayList<? super File> files = dir.getFiles();
            int size = files.size();
            for(int i = 0; i < size; i++) {
                File file = (File) files.get(i);
                if(file.getType() != "Directory")
                    if(size - i > 1)
                        filesTree += "  " + indent + "├─" + file.getName() + "\n";
                    else
                        filesTree += "  " + indent + "└─" + file.getName() + "\n";
                else
                {
                    if(size - i > 1) {
                        filesTree += "  " + indent + "├─" + file.getName() + "\n";
                        prettyFilesTree((Directory) file, indent + "|  ");
                    }
                    else
                    {
                        filesTree += "  " + indent + "└─" + file.getName() + "\n";
                        prettyFilesTree((Directory) file, indent + "  ");
                    }
                }
            }
        }
    }

    private static void prettyPrint(Directory dir, String indent) {
        if(dir.size() != 0) {
            ArrayList<? super File> files = dir.getFiles();
            int size = files.size();
            for(int i = 0; i < size; i++) {
                File file = (File) files.get(i);
                if(file.getType() != "Directory")
                    if(size - i > 1)
                        System.out.println("  " + indent + "├─" + file.getName());
                    else
                        System.out.println("  " + indent + "└─" + file.getName());
                else
                {
                    if(size - i > 1) {
                        System.out.println("  " + indent + "├─" + file.getName());
                        prettyPrint((Directory) file, indent + "|  ");
                    }
                    else
                    {
                        System.out.println("  " + indent + "└─" + file.getName());
                        prettyPrint((Directory) file, indent + "  ");
                    }
                }
            }
        }
    }
}
