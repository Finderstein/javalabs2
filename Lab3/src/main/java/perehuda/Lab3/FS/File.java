package perehuda.Lab3.FS;

public class File {
    private final String type;
    private String name;
    private Directory parent;

    public File(String type, String name, Directory parent) {
        this.type = type;
        this.name = name;
        this.parent = parent;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getType() {
        return type;
    }

    public String getName() {
        return name;
    }

    public synchronized Directory getParent() {
        return parent;
    }

    public synchronized boolean delete() {
        if(parent == null) return false;
        parent.removeFile(this);
        parent = null;
        return true;
    }

    public synchronized boolean moveTo(Directory dir) {
        parent.removeFile(this);
        parent = dir;
        return dir.addFile(this);
    }

    public String getPath() {
        if(parent == null) return '/' + name;
        return parent.getPath() + '/' + name;
    }
}
