package perehuda.Lab3.FS.ForkJoinPoolHelpers;

import perehuda.Lab3.FS.Directory;
import perehuda.Lab3.FS.File;

import java.util.ArrayList;
import java.util.concurrent.RecursiveTask;

public class LongestPathHelper extends RecursiveTask<String> {
    private final Directory directory;

    public LongestPathHelper(Directory directory) {
        this.directory = directory;
    }

    @Override
    protected String compute() {
        String path = directory.getPath();
        ArrayList<LongestPathHelper> subHelpers = new ArrayList<>();

        ArrayList<? super File> children = directory.getFiles();
        for(Object object : children) {
            File child = (File) object;

            if (child.getType() == "Directory") {
                LongestPathHelper helper = new LongestPathHelper((Directory)child);
                helper.fork();
                subHelpers.add(helper);
            }
        }

        for(LongestPathHelper helper : subHelpers) {
            String helperPath = helper.join();
            if(helperPath.chars().filter(ch -> ch == '/').count() > path.chars().filter(ch -> ch == '/').count()) path = helperPath;
        }

        return path;
    }
}