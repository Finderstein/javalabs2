package perehuda.Lab3;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import perehuda.Lab3.FS.Directory;
import perehuda.Lab3.FS.File;
import perehuda.Lab3.FS.LogFile;

class Response {
    public String result;

    public Response(String result) {
        this.result = result;
    }
}

@RestController
public class Controller {
    private Directory root = Directory.filledRoot();

    @GetMapping("/")
    public ResponseEntity<Response> start() {
        return ResponseEntity.ok(new Response("Welcome to the Lab 3!" +
                "\nThis lab is used to study spring web." +
                "\nThere are 5 functions:" +
                "\nGET: \"http://localhost:8080/\" - shows you possible requests;" +
                "\nGET: \"http://localhost:8080/root\" - shows you all files tree inside FS;" +
                "\nDELETE: \"http://localhost:8080/delete?name=name_of_file_to_delete\" - deletes file inside FS;" +
                "\nPUT: \"http://localhost:8080/put?name=name_of_file_to_change&newName=new_name_of_file\" - changes file name;" +
                "\nPOST: \"http://localhost:8080/put?name=name_of_log_file_to_append_data&data=data_that_will_be_appended\" - append data to log file;"));
    }

    @GetMapping("/root")
    public ResponseEntity<Response> root() {
        return ResponseEntity.ok(new Response(Directory.filesTree(root, "")));
    }

    @DeleteMapping("/delete")
    public ResponseEntity<Response> delete(@RequestParam(value = "name") String name) throws JsonProcessingException {
        File file = Directory.findInsideFS(root, name);
        return file != null && file.delete()
                ? ResponseEntity.ok(new Response("SUCCESS: File \"" + name + "\" successfully."))
                : new ResponseEntity<>(new Response("ERROR: Cannot find file \"" + name + "\" to delete!!!"), HttpStatus.BAD_REQUEST);
    }

    @PutMapping("/put")
    public ResponseEntity<Response> put(@RequestParam(value = "name") String name, @RequestParam(value = "newName") String newName) {
        File file = Directory.findInsideFS(root, name);
        if(file != null) file.setName(newName);
        return file != null && file.getName().contains(newName)
                ? ResponseEntity.ok(new Response("SUCCESS: File \"" + name + "\" renamed to \"" + newName + "\" successfully."))
                : new ResponseEntity<>(new Response("ERROR: Cannot find file \"" + name + "\" to rename!!!"), HttpStatus.BAD_REQUEST);
    }

    @PostMapping("/post")
    public ResponseEntity<Response> post(@RequestParam(value = "name") String name, @RequestParam(value = "data") String data) {
        File file = Directory.findInsideFS(root, name);
        return file != null && file.getType().contains("LogFile") && ((LogFile)file).append(data) && ((LogFile)file).getLogs().contains(data)
                ? ResponseEntity.ok(new Response("SUCCESS: File \"" + name + "\" appended with data \"" + data + "\"."))
                : new ResponseEntity<>(new Response("ERROR: Cannot find log file \"" + name + "\"!!!"), HttpStatus.BAD_REQUEST);
    }
}