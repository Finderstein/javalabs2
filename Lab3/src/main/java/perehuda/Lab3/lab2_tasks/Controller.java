package perehuda.Lab3.lab2_tasks;

import perehuda.Lab3.FS.FSActions.FSAction;

import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;
import java.util.concurrent.Future;

public class Controller {
    ExecutorService workers;

    public Controller(int amountOfWorkerThreads) {
        workers = Executors.newFixedThreadPool(amountOfWorkerThreads);
    }


    public <T>Future<T> submitFSAction (FSAction<T> action) {
        try {
            return workers.submit(action::execute);
        } catch (Exception e){
            System.out.println(e.getMessage());
            throw e;
        }
    }

    public void stop(){
        workers.shutdown();
    }
}
