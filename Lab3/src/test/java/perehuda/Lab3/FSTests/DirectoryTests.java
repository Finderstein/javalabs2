package perehuda.Lab3.FSTests;

import org.junit.jupiter.api.Test;
import perehuda.Lab3.FS.*;

import static org.junit.jupiter.api.Assertions.*;

public class DirectoryTests {
    @Test
    void createDirectoryTest() {
        Directory dir = Directory.create("Abrakadabra", null);

        assertNotNull(dir);
        assertEquals("Abrakadabra", dir.getName());
        assertEquals("Directory", dir.getType());
        assertNull(dir.getParent());
        assertNull(Directory.create("", null));
        assertNull(Directory.create("/", dir));
    }

    @Test
    void addFileToDirectoryTest() {
        Directory root = Directory.create("Abrakadabra", null);
        BufferFile file1 = BufferFile.<Integer>create("file1", root);
        BinaryFile file2 = BinaryFile.create("file2", root, "info");
        Directory dir = Directory.create("dir", root);

        assertTrue(root.getFiles().contains(file1));
        assertTrue(root.getFiles().contains(file2));
        assertTrue(root.getFiles().contains(dir));
    }

    @Test
    void removeFileFromDirectoryTest() {
        Directory root = Directory.create("Abrakadabra", null);
        BufferFile file1 = BufferFile.<Integer>create("file1", root);
        BinaryFile file2 = BinaryFile.create("file2", root, "data");
        Directory dir = Directory.create("dir", root);

        assertTrue(root.getFiles().contains(file1));
        assertTrue(root.getFiles().contains(file2));
        assertTrue(root.getFiles().contains(dir));

        file2.delete();

        assertTrue(root.getFiles().contains(file1));
        assertFalse(root.getFiles().contains(file2));
        assertTrue(root.getFiles().contains(dir));
    }

    @Test
    void moveDirectoryTest() {
        Directory root = Directory.create("root", null);
        Directory subDir1 = Directory.create("subDir1", root);
        Directory subDir2 = Directory.create("subDir2", root);
        BinaryFile file = BinaryFile.create("file", subDir1, "info");

        assertTrue(root.getFiles().contains(subDir1));
        assertTrue(root.getFiles().contains(subDir2));
        assertTrue(subDir1.getFiles().contains(file));

        subDir1.moveTo(subDir2);

        assertFalse(root.getFiles().contains(subDir1));
        assertTrue(root.getFiles().contains(subDir2));
        assertTrue(subDir2.getFiles().contains(subDir1));
        assertTrue(subDir1.getFiles().contains(file));
    }

    @Test
    void deleteDirectoryTest() {
        Directory root = Directory.create("root", null);
        Directory subDir1 = Directory.create("subDir1", root);
        Directory subDir2 = Directory.create("subDir2", root);

        subDir1.delete();

        assertFalse(root.getFiles().contains(subDir1));
        assertTrue(root.getFiles().contains(subDir2));
    }
}
