package perehuda.Lab3.FSTests;

import org.junit.jupiter.api.Test;
import perehuda.Lab3.FS.BufferFile;
import perehuda.Lab3.FS.Directory;

import static org.junit.jupiter.api.Assertions.*;
import static org.junit.jupiter.api.Assertions.assertFalse;

public class BufferFileTests {
    @Test
    void createBufferFileTest() {
        Directory root = Directory.create("root", null);
        BufferFile file = BufferFile.<Integer>create("file", root);

        assertNull(BufferFile.<Integer>create("Abrakadabra", null));
        assertNull(BufferFile.<Integer>create("", root));
        assertNull(BufferFile.<Integer>create("/", root));

        assertNotNull(file);
        assertEquals("file", file.getName());
        assertEquals("BufferFile", file.getType());
    }

    @Test
    void moveBufferFileTest() {
        Directory root = Directory.create("root", null);
        Directory subDir1 = Directory.create("subDir1", root);
        Directory subDir2 = Directory.create("subDir2", root);
        BufferFile file = BufferFile.<Integer>create("file", subDir1);

        assertTrue(root.getFiles().contains(subDir1));
        assertTrue(root.getFiles().contains(subDir2));
        assertTrue(subDir1.getFiles().contains(file));

        file.moveTo(subDir2);

        assertFalse(subDir1.getFiles().contains(file));
        assertTrue(root.getFiles().contains(subDir2));
        assertTrue(root.getFiles().contains(subDir1));
        assertTrue(subDir2.getFiles().contains(file));
    }

    @Test
    void deleteBufferFileTest() {
        Directory root = Directory.create("root", null);
        Directory subDir1 = Directory.create("subDir1", root);
        BufferFile file1 = BufferFile.<Integer>create("file1", root);
        BufferFile file2 = BufferFile.<Integer>create("file2", subDir1);

        assertTrue(root.getFiles().contains(file1));
        assertTrue(subDir1.getFiles().contains(file2));

        file1.delete();
        file2.delete();

        assertFalse(root.getFiles().contains(file1));
        assertFalse(subDir1.getFiles().contains(file2));
    }

    @Test
    void consumeBufferFileTest() {
        Directory root = Directory.create("root", null);
        BufferFile file = BufferFile.<Integer>create("file", root);

        assertEquals(null, file.consume());

        file.push(13);
        file.push(25);

        assertEquals(13, file.consume());
        assertEquals(25, file.consume());
    }
}
