package perehuda.Lab3.FSTests;

import org.junit.jupiter.api.AfterEach;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import perehuda.Lab3.FS.*;
import perehuda.Lab3.FS.FSActions.*;
import perehuda.Lab3.lab2_tasks.Controller;

import java.util.ArrayList;
import java.util.concurrent.ExecutionException;

import static org.junit.jupiter.api.Assertions.*;

public class FSActionsTests {
    Controller controller;
    Directory root;
    Directory subDir;
    BinaryFile bin;
    BufferFile buff;
    LogFile log;
    MyFile myFile;

    @BeforeEach
    void createEssentials() {
        root = Directory.create("root", null);
        subDir = Directory.create("subDir", root);
        bin = BinaryFile.create("bin", root, "abra");
        buff = BufferFile.<String>create("buff", subDir);
        log = LogFile.create("log", root, "data");
        myFile = MyFile.create("myFile", subDir, "13");
        controller = new Controller(10);
    }

    @Test
    void addAction() throws ExecutionException, InterruptedException {
        assertTrue(controller.submitFSAction(new Add(myFile, "24")).get());
        assertTrue(controller.submitFSAction(new Add(myFile, "7")).get());
        assertEquals("44", myFile.getNum());
    }

    @Test
    void addFileAction() throws ExecutionException, InterruptedException {
        assertTrue(controller.submitFSAction(new AddFile(root, new File("Test", "file1", root))).get());
        assertTrue(controller.submitFSAction(new AddFile(root, new File("Test", "file2", subDir))).get());
    }

    @Test
    void appendAction() throws ExecutionException, InterruptedException {
        assertEquals(true, controller.submitFSAction(new Append(log, " More data")).get());
        assertEquals(true, controller.submitFSAction(new Append(log, " Even more data")).get());
        assertEquals("data More data Even more data", log.getLogs());
    }

    @Test
    void consumeAndPushAction() throws ExecutionException, InterruptedException {
        assertTrue(controller.submitFSAction(new Push<String>(buff, "data1")).get());
        assertTrue(controller.submitFSAction(new Push<String>(buff, "data2")).get());
        assertEquals("data1", controller.submitFSAction(new Consume<String>(buff)).get());
        assertEquals("data2", controller.submitFSAction(new Consume<String>(buff)).get());
    }

    @Test
    void countAction() throws ExecutionException, InterruptedException {
        assertEquals((Integer) 3, controller.submitFSAction(new Count(root, false)).get());
        assertEquals((Integer) 5, controller.submitFSAction(new Count(root, true)).get());
    }

    @Test
    void createAction() throws ExecutionException, InterruptedException {
        BinaryFile testCreate1 = (BinaryFile) controller.submitFSAction(new Create(root, "testbin", "BinaryFile", "kadabra")).get();
        assertEquals("testbin", testCreate1.getName());
        assertEquals("BinaryFile", testCreate1.getType());
        assertEquals("kadabra", testCreate1.getData());

        Directory testCreate2 = (Directory) controller.submitFSAction(new Create(root, "testdir", "Directory")).get();
        assertEquals("testdir", testCreate2.getName());
        assertEquals("Directory", testCreate2.getType());
    }

    @Test
    void deleteAction() throws ExecutionException, InterruptedException {
        assertEquals(true, controller.submitFSAction(new Delete(bin)).get());
        assertFalse(root.getFiles().contains(bin));
        assertEquals(true, controller.submitFSAction(new Delete(subDir)).get());
        assertFalse(root.getFiles().contains(subDir));
        assertFalse(root.getFiles().contains(buff));
    }

    @Test
    void fullAction() throws ExecutionException, InterruptedException {
        assertFalse(controller.submitFSAction(new Full(root)).get());

        for(int i = 0; i < 17; i++) {
            assertTrue(controller.submitFSAction(new AddFile(root, new File("Test", "file" + i, root))).get());
        }

        assertTrue(controller.submitFSAction(new Full(root)).get());
    }

    @Test
    void getBinaryDataAction() throws ExecutionException, InterruptedException {
        assertEquals("abra", controller.submitFSAction(new GetBinaryData(bin)).get());
        assertEquals("kadabra", controller.submitFSAction(new GetBinaryData(BinaryFile.create("test", root, "kadabra"))).get());
    }

    @Test
    void getFilesAction() throws ExecutionException, InterruptedException {
        assertEquals(root.getFiles(), controller.submitFSAction(new GetFiles(root)).get());
        assertEquals(subDir.getFiles(), controller.submitFSAction(new GetFiles(subDir)).get());
    }

    @Test
    void getLogDataAction() throws ExecutionException, InterruptedException {
        assertEquals("data", controller.submitFSAction(new GetLogData(log)).get());
        log.append("1234");
        assertEquals("data1234", controller.submitFSAction(new GetLogData(log)).get());
    }

    @Test
    void getNameAction() throws ExecutionException, InterruptedException {
        assertEquals("root", controller.submitFSAction(new GetName(root)).get());
        assertEquals("bin", controller.submitFSAction(new GetName(bin)).get());
    }

    @Test
    void getNumAction() throws ExecutionException, InterruptedException {
        assertEquals("13", controller.submitFSAction(new GetNum(myFile)).get());
        myFile.add("7");
        assertEquals("20", controller.submitFSAction(new GetNum(myFile)).get());
    }

    @Test
    void getParentAction() throws ExecutionException, InterruptedException {
        assertEquals(root, controller.submitFSAction(new GetParent(bin)).get());
        assertEquals(subDir, controller.submitFSAction(new GetParent(buff)).get());
        buff.moveTo(root);
        assertFalse(subDir.getFiles().contains(buff));
        assertEquals(root, controller.submitFSAction(new GetParent(buff)).get());
    }

    @Test
    void getTypeAction() throws ExecutionException, InterruptedException {
        assertEquals("Directory", controller.submitFSAction(new GetType(root)).get());
        assertEquals("BufferFile", controller.submitFSAction(new GetType(buff)).get());
    }

    @Test
    void longestPathAction() throws ExecutionException, InterruptedException {
        assertEquals("/root/subDir", controller.submitFSAction(new LongestPath(root)).get());
        Directory.create("test", subDir);
        assertEquals("/root/subDir/test", controller.submitFSAction(new LongestPath(subDir)).get());
    }

    @Test
    void moveToAction() throws ExecutionException, InterruptedException {
        assertTrue(controller.submitFSAction(new MoveTo(bin, subDir)).get());
        assertTrue(subDir.getFiles().contains(bin));
        assertTrue(controller.submitFSAction(new MoveTo(myFile, root)).get());
        assertTrue(root.getFiles().contains(myFile));
    }

    @Test
    void removeFileAction() throws ExecutionException, InterruptedException {
        assertTrue(controller.submitFSAction(new RemoveFile(root, bin)).get());
        assertFalse(root.getFiles().contains(bin));
        assertTrue(controller.submitFSAction(new RemoveFile(root, subDir)).get());
        assertFalse(root.getFiles().contains(subDir));
        assertFalse(root.getFiles().contains(buff));
    }

    @Test
    void searchAction() throws ExecutionException, InterruptedException {
        ArrayList<String> search = new ArrayList<>();
        search.add("/root/subDir");
        assertEquals(search, controller.submitFSAction(new Search(root, "subDir")).get());
        search.add("/root/bin");
        search.add("/root/subDir/buff");
        assertEquals(search, controller.submitFSAction(new Search(root, "b")).get());
    }

    @Test
    void treeAction() throws ExecutionException, InterruptedException {
        assertNotNull(controller.submitFSAction(new Tree(root)).get());
        assertEquals(String.class, controller.submitFSAction(new Tree(root)).get().getClass());
    }

    @AfterEach
    void stopController() {
        controller.stop();
    }
}
