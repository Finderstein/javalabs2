package perehuda.Lab3;

import com.fasterxml.jackson.databind.ObjectMapper;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.web.servlet.MockMvc;
import perehuda.Lab3.FS.Directory;

import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.*;
import static org.springframework.test.web.servlet.result.MockMvcResultHandlers.print;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.content;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

@SpringBootTest
@AutoConfigureMockMvc
public class Lab3ApplicationTests {
	@Autowired
	private MockMvc mockMvc;
	ObjectMapper mapper = new ObjectMapper();

	@Test
	public void A1GetStart() throws Exception {
		this.mockMvc.perform(get("/")).andDo(print())
				.andExpect(status().isOk())
				.andExpect(content().string(mapper.writeValueAsString(new Response("Welcome to the Lab 3!" +
						"\nThis lab is used to study spring web." +
						"\nThere are 5 functions:" +
						"\nGET: \"http://localhost:8080/\" - shows you possible requests;" +
						"\nGET: \"http://localhost:8080/root\" - shows you all files tree inside FS;" +
						"\nDELETE: \"http://localhost:8080/delete?name=name_of_file_to_delete\" - deletes file inside FS;" +
						"\nPUT: \"http://localhost:8080/put?name=name_of_file_to_change&newName=new_name_of_file\" - changes file name;" +
						"\nPOST: \"http://localhost:8080/put?name=name_of_log_file_to_append_data&data=data_that_will_be_appended\" - append data to log file;"))));
	}

	@Test
	public void B2GetRoot() throws Exception {
		Directory root = Directory.filledRoot();
		this.mockMvc.perform(get("/root")).andDo(print())
				.andExpect(status().isOk());
	}

	@Test
	public void CC3Delete() throws Exception {
		this.mockMvc.perform(delete("/delete?name=BinFile6.bin")).andDo(print())
				.andExpect(status().isOk())
				.andExpect(content().string(mapper.writeValueAsString(new Response("SUCCESS: File \"BinFile6.bin\" successfully."))));
	}

	@Test
	public void D4Put() throws Exception {
		this.mockMvc.perform(put("/put?name=MyFile2.my&newName=newFile.my")).andDo(print())
				.andExpect(status().isOk())
				.andExpect(content().string(mapper.writeValueAsString(new Response("SUCCESS: File \"MyFile2.my\" renamed to \"newFile.my\" successfully."))));
	}

	@Test
	public void E5Post() throws Exception {
		this.mockMvc.perform(post("/post?name=LogFile1.log&data=new line")).andDo(print())
				.andExpect(status().isOk())
				.andExpect(content().string(mapper.writeValueAsString(new Response("SUCCESS: File \"LogFile1.log\" appended with data \"new line\".")).toString()));
	}
}